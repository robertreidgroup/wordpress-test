## Wordpress Task

In order to be considered for the Wordpress position, you must complete the following steps. 

*Note: This task should take no longer than 1 to 2 hours at most.*

### Prerequisites

- Know how to use GIT
- Be able to run a local installation of Wordpress

## Task

1. Fork this repository (if you don't know how to do that, Google is your friend): https://bitbucket.org/benohear/wordpress-test 
2. Install on your local machine, using the database dump located in data/wp_test.sql
    - Admin login is admin / admin
    - Tip: You’ll need to replace all occurrences of wp_test:8888 in the database to whatever url you are running your installation from and tweak wp-config.php to match your database details.
3. Select a plugin that matches the functionality of this event calendar the closest: http://www.foofwa.com/calendar/index.html 
    - **Important:** 
        - It is unlikely that you will find an exact match. That doesn’t matter (see point #5). 
        - This is not about the look and feel. Only the functionality.
4. Install the plugin and add three dummy events
5. Open the text file in “comments/calendar_notes.txt” and
    - Tell us where the example entries and the plugin administration can be found
    - Document any differences between your selected plugin and the current calendar.
    - Explain what you would do next to make this perfect. For example: 
        - Customizing the plugin
        - Complete rewrite or
        - Making the case the plugin functionality is in fact better. 
        - Or a mix.
6. Export the database and overwrite data/wp_test.sql with the dump.
7. Commit and Push your code to your new repository
8. Send us a pull request, we will review your code and get back to you

